'use strict'

/**
 * Component used to display the list of books.
 * @type {Object}
 */
module.exports = {
  /**
   * Component's props.
   * @type {Array.<string>}
   */
  props: [ 'books' ],

  /**
   * Component's template.
   * @type {string}
   */
  template: require('./template.html'),

  /**
   * Component's computed properties.
   * @type {Object}
   */
  computed: {
    /**
     * Indicates whether to display the message informing the user that no books
     * were found matching the search criteria.
     * @return {boolean} TRUE, if the message should be displayed.
     * @return {boolean} FALSE, if the message should not be displayed.
     */
    displayNoBooksMessage () {
      return this.books.length === 0
    }
  },

  /**
   * Component's methods.
   * @type {Object}
   */
  methods: {
    /**
     * Function used as a listener to the scroll event emitted by the HTML
     * element displaying the list of books, so that when the user scrolls all
     * the way down, it emits an event that the parent component can use to load
     * more data.
     * @param  {Object} ev Event object.
     */
    onScroll (ev) {
      /**
       * Offset from the bottom of the page when the event should be emitted.
       * @type {number}
       */
      const offset = 20

      /**
       * Current scroll position.
       * @type {number}
       */
      const scrollTop = ev.target.scrollTop

      /**
       * Height of the HTML Element's viewport.
       * @type {number}
       */
      const offsetHeight = ev.target.offsetHeight

      /**
       * Scroll height of the HTML Element.
       * @type {number}
       */
      const scrollHeight = ev.target.scrollHeight

      // Stop from emitting the event, if the user has not scrolled down enough.
      if (scrollHeight - (scrollTop + offsetHeight + offset) > 0) return

      // Emit event.
      this.$emit('pull')
    }
  }
}
