'use strict'

const Vue = require('vue')

/**
 * List of components available to the Vue App.
 * @type {Array.<Object>}
 */
module.exports = [
  'book',
  'loading',
  'utilities',
  'books-list',
  'book-sorter',
  'book-filter',
  'filter-input-field',
  'filter-select-field'
].reduce((components, name) => {
  // Retrieve the component.
  const component = require(`./${name}`)

  // Register the component globally with Vue.
  Vue.component(name, component)

  // Index the component by its name.
  components[name] = component

  // Return components object to be used in the next iteration or exported by
  // this module.
  return components
}, [])
