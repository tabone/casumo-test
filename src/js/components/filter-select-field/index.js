'use strict'

/**
 * Component used to accept filter values as options.
 * @type {Object}
 */
module.exports = {
  /**
   * Component's props.
   * @type {Array.<string>}
   */
  props: [ 'opts' ],

  /**
   * Component's template.
   * @type {string}
   */
  template: require('./template.html'),

  /**
   * Hook invoked when the component is created.
   */
  created () {
    // Reset value to null.
    this.$emit('updated', null)
  },

  /**
   * Component's data.
   */
  data () {
    return {
      /**
       * Filter value entered by the user.
       * @type {string}
       */
      value: null
    }
  },

  /**
   * Component's watchers.
   * @type {Object}
   */
  watch: {
    /**
     * Watch for property used to store the components options props, so that
     * when they are updated, it resets the filter value.
     */
    opts () {
      this.value = null
    },

    /**
     * Watch for property used to store the filter value, so that when it is
     * updated, it notifies the listeners.
     */
    value () {
      this.$emit('updated', this.value)
    }
  }
}
