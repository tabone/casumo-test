'use strict'

/**
 * Component used to handle the sort & filter features of the app.
 * @type {Object}
 */
module.exports = {
  /**
   * Component's template.
   * @type {string}
   */
  template: require('./template.html'),

  /**
   * Component's methods.
   * @type {Object}
   */
  methods: {
    /**
     * Function to be used as a listener for the event emitted when the user
     * updates the sort info. This function transforms the data and relays the
     * same event to the parent component.
     * @param  {Object} data Sort info.
     */
    sortUpdated (data) {
      this.$emit('updated', { type: 'sort', data })
    },

    /**
     * Function to be used as a listener for the event emitted when the user
     * updates the filter info. This function transforms the data and relays the
     * same event to the parent component.
     * @param  {Object} data Sort info.
     */
    filterUpdated (data) {
      this.$emit('updated', { type: 'filter', data })
    }
  }
}
