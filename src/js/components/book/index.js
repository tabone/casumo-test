'use strict'

/**
 * Component used to represent a single book.
 * @type {Object}
 */
module.exports = {
  /**
   * Component's props.
   * @type {Array.<string>}
   */
  props: [ 'book' ],

  /**
   * Component's template.
   * @type {string}
   */
  template: require('./template.html'),

  /**
   * Component's data.
   * @return {Object} Component's data.
   */
  data () {
    return {
      /**
       * Book's title.
       * @type {string}
       */
      title: this.book.title,

      /**
       * Book's cover image.
       * @type {string}
       */
      cover: this.book.image,

      /**
       * Book's genre.
       * @type {string}
       */
      genre: this.book.genre,

      /**
       * Book author's name.
       * @type {string}
       */
      author: this.book.author.name,

      /**
       * Book's published date.
       * @type {string}
       */
      published: getPublishedDate(this.book.publishedDate),

      /**
       * Classname to be used on the HTML Element displaying the Book author's
       * name to indicate his/her gender.
       * @type {string}
       */
      genderClassName: this.book.author.gender === 'M'
        ? 'app-book__field--male-author' : 'app-book__field--female-author'
    }
  },

  /**
   * Components computed properties.
   * @type {Object}
   */
  computed: {
    /**
     * Classname for the HTML Element displaying the cover.
     * @type {string}
     */
    coverClassName () {
      const pub = new Date(this.book.publishedDate)
      if (isHalloweenBook(this, pub)) return 'app-book__cover--halloween'
      if (isFinanceBook(this, pub)) return 'app-book__cover--finance'
      return null
    }
  }
}

/**
 * Function used to format the published date of a book.
 * @param  {string} date Published date retrieved from API.
 * @return {string}      Formatted published date.
 */
function getPublishedDate (date) {
  const pub = new Date(date)
  return `${pub.getDate()} / ${pub.getMonth() + 1} / ${pub.getFullYear()}`
}

/**
 * Function used to check whether a horror book has been published on Halloween
 * day.
 * @param  {Object}  self Component's object.
 * @param  {string}  date Book's published date.
 * @return {boolean}      TRUE if the specified book is a horror book that has
 *                        been published on Halloween day.
 * @return {boolean}      FALSE if the specified book is either not a horror
 *                        book or has not been published on Halloween day.
 */
function isHalloweenBook (self, date) {
  return self.genre === 'Horror' &&
    date.getDate() === 31 &&
    date.getMonth() === 9
}

/**
 * Function used to check whether a finance book has been published on the last
 * friday of a month.
 * @param  {Object}  self Component's object.
 * @param  {string}  date Book's published date.
 * @return {boolean}      TRUE if the specified book is a finance book that has
 *                        been published on the last friday of a month.
 * @return {boolean}      FALSE if the specified book is eithr not a finance
 *                        book or has not been published on the last friday of a
 *                        month.
 */
function isFinanceBook (self, date) {
  return self.genre === 'Finance' &&
    date.getDate() === getLastFriday(date.getFullYear(), date.getMonth())
    .getDate()
}

/**
 * Function used to return the date of the last friday of a month.
 * @param  {number} year  Year.
 * @param  {number} month Month.
 * @return {Date}       Last friday of the specified month.
 */
function getLastFriday (year, month) {
  // First we'll start by creating a date object representing the last day of
  // the specified month.
  const lastFriday = new Date(year, month + 1)
  lastFriday.setDate(0)

  // Next based on the day of the last day we will subtract to get to the last
  // friday of the month.
  const diff = (lastFriday.getDay() >= 5)
    ? lastFriday.getDay() - 5
    : lastFriday.getDay() + 2

  lastFriday.setDate(lastFriday.getDate() - diff)

  // Return last friday date.
  return lastFriday
}
