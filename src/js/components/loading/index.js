'use strict'

/**
 * Component used to display a loading message when the app is waiting for a
 * response by the api.
 * @type {Object}
 */
module.exports = {
  /**
   * Component's props.
   * @type {Array.<string>}
   */
  props: [ 'show' ],

  /**
   * Component's template.
   * @type {string}
   */
  template: require('./template.html'),

  /**
   * Component's computed properties.
   * @type {Object}
   */
  computed: {
    /**
     * Classname to be used on the component's root HTML Element so that it is
     * only displayed when the app is waiting for a response.
     * @type {string}
     */
    className () {
      return (this.show === true) ? 'app-loading--show' : ''
    }
  }
}
