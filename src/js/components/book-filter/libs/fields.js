'use strict'

/**
 * List of options to be used in the dropdown field displaying the fields the
 * user can filter with.
 * @type {Array.<Object>}
 */
module.exports = [{
  label: 'Filter By',
  opts: null,
  query: null,
  component: null
}, {
  label: 'Book Title',
  opts: {},
  query: '[book][title]',
  component: 'filter-input-field'
},
{
  label: 'Book Genre',
  query: '[book][genre]',
  component: 'filter-select-field',
  opts: {
    options: [{
      label: 'Select Genre',
      value: null
    }, {
      label: 'Horror',
      value: 'horror'
    }, {
      label: 'Finance',
      value: 'finance'
    }, {
      label: 'Fiction',
      value: 'fiction'
    }, {
      label: 'Satire',
      value: 'satire'
    }, {
      label: 'Romance',
      value: 'romance'
    }]
  }
},
{
  label: 'Author Name',
  opts: {},
  query: '[author][name]',
  component: 'filter-input-field'
},
{
  label: 'Author Gender',
  query: '[author][gender]',
  component: 'filter-select-field',
  opts: {
    options: [{
      label: 'Select Gender',
      value: null
    }, {
      label: 'Male',
      value: 'M'
    }, {
      label: 'Female',
      value: 'F'
    }]
  }
}]
