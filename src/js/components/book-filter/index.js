'use strict'

const fields = require('./libs/fields')

/**
 * Component used to enable the user to filter the list of books.
 * @type {Object}
 */
module.exports = {
  /**
   * Component's template.
   * @type {string}
   */
  template: require('./template.html'),

  /**
   * Component's data.
   * @return {Object} Component's data.
   */
  data () {
    return {
      /**
       * List of options to be used in the dropdown field displaying the fields
       * the user can filter with.
       * @type {Array.<Object>}
       */
      fields,

      /**
       * Field to be filtered.
       * @type {string}
       */
      selectedField: fields[0]
    }
  },

  /**
   * Component's computed properties.
   * @type {Object}
   */
  computed: {
    /**
     * Classname to be used on the DOM element containing the field accepting
     * the filter value to display it only when a filter field has been
     * selected.
     * @type {string}
     */
    filerValueClassName () {
      return (this.selectedField.component === null)
        ? ''
        : 'app-book-filter--show-value'
    }
  },

  /**
   * Component's watchers.
   * @type {Object}
   */
  watch: {
    /**
     * Watcher for property used to store the field that the filtering will be
     * made on, so that when it is updated it notifies the listeners.
     * @param  {object} newField Newly selected filter field.
     */
    selectedField (newField) {
      this.$emit('updated', { type: 'field', data: newField.query })
    }
  },

  /**
   * Component's methods.
   * @type {Object}
   */
  methods: {
    /**
     * Function used as a listener for the event emitted when the filter value
     * is updated. This function transforms the data and relays the same event
     * to the parent component.
     * @param  {string} newValue New filter value.
     */
    updatedValue (newValue) {
      this.$emit('updated', { type: 'value', data: newValue })
    }
  }
}
