'use strict'

const fields = require('./libs/fields')

/**
 * Component used to enable the user to sort the list of books.
 * @type {Object}
 */
module.exports = {
  /**
   * Component's template.
   * @type {string}
   */
  template: require('./template.html'),

  /**
   * Component's data.
   * @return {Object} Component's data.
   */
  data () {
    return {
      /**
       * List of options to be used in the dropdown field displaying the fields
       * the user can sort with.
       * @type {Array.<Object>}
       */
      fields,

      /**
       * Sort order.
       * @type {noolean}
       */
      order: false,

      /**
       * Field to be sorted.
       * @type {string}
       */
      selectedField: fields[0]
    }
  },

  /**
   * Component's computed properties.
   * @type {Object}
   */
  computed: {
    /**
     * Indicates whether to display the sort order icon. The sort order icon
     * should be displayed when the user has selected a field to sort with.
     * @type {boolean}
     */
    displaySortIcon () {
      return this.selectedField.query !== null
    },

    /**
     * Google Material icon to use to indicate the current sort order.
     * @type {string}
     */
    sortIcon () {
      return (this.order === true) ? 'arrow_drop_up' : 'arrow_drop_down'
    }
  },

  /**
   * Component's watchers.
   * @type {Object}
   */
  watch: {
    /**
     * Watcher for property used to store the sort order, so that when it is
     * updated it notifies the listeners.
     * @param  {string} newValue New filter value.
     */
    order (newOrder) {
      this.$emit('updated', { type: 'order', data: newOrder })
    },

    /**
     * Watcher for property used to store the field that the sorting will be
     * made on, so that when it is updated it notifies the listeners.
     * @param  {string} newValue New filter value.
     */
    selectedField (newField) {
      this.$emit('updated', { type: 'field', data: newField.query })
    }
  },

  /**
   * Component's methods.
   * @type {Object}
   */
  methods: {
    /**
     * Function used to toggle the sort order.
     */
    toggleSortOrder () {
      this.order = !this.order
    }
  }
}
