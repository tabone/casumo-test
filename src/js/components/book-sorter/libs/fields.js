'use strict'

/**
 * List of options to be used in the dropdown field displaying the fields the
 * user can sort with.
 * @type {Array.<Object>}
 */
module.exports = [
  { label: 'Sort By', query: null },
  { label: 'Book Title', query: '[book][title]' },
  { label: 'Author Name', query: '[author][name]' }
]
