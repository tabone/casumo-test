'use strict'

require('./components')

const Vue = require('vue')

// Create Vue instance.
const app = new Vue({
  /**
   * Classname reference to the HTML Element to serve as the template for the
   * RootViewModel.
   * @type {string}
   */
  el: '.app',

  /**
   * Lifecycle hook invoked when the Vue object is created.
   * @return {Promise} Promise resolved once either the data has been
   *                   successfully retrieved or the error handled.
   */
  created () {
    return fetchData(this)
  },

  /**
   * RootViewModel's data.
   * @type {Object}
   */
  data: {
    /**
     * Stores the list of books being displayed in the app.
     * @type {Array.<Object>}
     */
    books: [],

    /**
     * Indicates whether the app is waiting for a response from the API Server.
     * @type {boolean}
     */
    isLoading: false,

    /**
     * ID of the timeout created to queue a request to retrieve the list of
     * books based on the current pagination, sort & filter info. This id is
     * stored so that if the user changes the search criteria before the current
     * queded request is done it resets the timer.
     * @type {number}
     */
    timeoutID: null,

    /**
     * Current page.
     * @type {number}
     */
    page: 1,

    /**
     * Stores filter info.
     * @type {Object}
     */
    filter: {
      /**
       * Filter value.
       * @type {string}
       */
      value: null,

      /**
       * Query parameter referencing the field that the filtering will be made
       * on.
       * @example
       * => '[book][title]'
       * @type {string}
       */
      field: null
    },

    /**
     * Stores sort info.
     * @type {Object}
     */
    sort: {
      /**
       * Query parameter referencing the field that the sort will be made on.
       * @example
       * => '[author][name]'
       * @type {string}
       */
      field: null,

      /**
       * Sort order.
       * @type {boolean}
       */
      order: false
    }
  },

  /**
   * RootViewModel's computed properties.
   * @type {Object}
   */
  computed: {
    /**
     * Query parameters for pagination.
     * @type {string}
     */
    pageQueryParam () {
      /**
       * Number of books to return per page.
       * @type {number}
       */
      const recordsPerPage = 10

      // Calculate pagination offset based on current page & number of books to
      // return per page.
      const offset = (this.page - 1) * recordsPerPage

      // Return pagination query string.
      return `[page][offset]=${offset}&[page][limit]=${recordsPerPage}`
    },

    /**
     * Query parameters for sorting.
     * @type {string}
     */
    sortQueryParam () {
      /**
       * Stores the sort info.
       * @type {Object}
       */
      const sort = this.sort

      // Stop process & return null, if the user hasn't specified a field to
      // sort with.
      if (sort.field === null) return null

      // Return sort query string.
      return `sort${sort.field}=${(sort.order === true) ? 'asc' : 'desc'}`
    },

    /**
     * Query parameters for filtering.
     * @type {string}
     */
    filterQueryParam () {
      /**
       * Stores the filter info.
       * @type {Object}
       */
      const filter = this.filter

      // Stop process & return null, if the user has either not entered a filter
      // value or selected a field to filter on.
      if (filter.field === null || filter.value === null) return null

      // Return filter query string.
      return `filter${filter.field}=${filter.value}`
    },

    /**
     * Query parameters to be used based on the current filter, sort &
     * pagination info.
     * @type {string}
     */
    queryParams () {
      /**
       * Array used to store the query params to be used based on the current
       * filter, sort & pagination info.
       * @type {Array.<string>}
       */
      let queryParams = []

      /**
       * Pagination query params.
       * @type {string}
       */
      const pageQueryParam = this.pageQueryParam

      /**
       * Sort query params.
       * @type {string}
       */
      const sortQueryParam = this.sortQueryParam

      /**
       * Filter query params.
       * @type {string}
       */
      const filterQueryParam = this.filterQueryParam

      // Include the query params that should be included based on the current
      // criteria info.
      if (pageQueryParam !== null) queryParams.push(pageQueryParam)
      if (sortQueryParam !== null) queryParams.push(sortQueryParam)
      if (filterQueryParam !== null) queryParams.push(filterQueryParam)

      // Return an empty string if there are no query parameters to be used.
      if (queryParams.length === 0) return ''

      // Return query parameters.
      return `?${queryParams.join('&')}`
    }
  },

  /**
   * RootViewModel's watchers.
   * @type {Object}
   */
  watch: {
    /**
     * Watcher for the computed property used to return the sort query
     * parameters, so that when it is updated, it resets the pagination back to
     * the first page.
     */
    sortQueryParam () {
      this.page = 1
    },

    /**
     * Watcher for the computed property used to return the filter query
     * parameters, so that when it is updated, it resets the pagination back to
     * the first page.
     */
    filterQueryParam () {
      this.page = 1
    },

    /**
     * Watcher for the computed property used to return the query parameters, so
     * that when it is updated, it resets the timer for the fetch request.
     */
    queryParams () {
      // Clear current queued fetch request, if there is any.
      window.clearTimeout(this.timeoutID)

      // Enqueue a new fetch request.
      this.timeoutID = window.setTimeout(() => fetchData(this), 1000)
    }
  },

  /**
   * RootViewModel's methods.
   * @type {Object}
   */
  methods: {
    /**
     * Function used as a listener for the event emitted when the user scrolls
     * to the bottom of the book list. This listener is used to increment the
     * current page.
     */
    updateCurrentPage () {
      if (this.isLoading === false && this.timeoutID === null) this.page += 1
    },

    /**
     * Function used as a listener for the event emitted when the user updates
     * the sort or filter info. This listener is used to store the new sort &
     * filter info.
     * @param  {Object} info Stores the new sort or filter info.
     */
    updatedCriteria (info) {
      switch (info.type) {
        case 'sort': return handleSortChange(info.data, this)
        case 'filter': return handleFilterChange(info.data, this)
      }
    }
  }
})

/**
 * Function used to retrieve books based on the current sort, fitler &
 * pagination info.
 * @param  {Object} self RootViewModel object.
 * @return {Promise}     Promise resolved once either the list of books have
 *                       been successfully retrieved or the error handled.
 */
function fetchData (self) {
  // Indicate that the app is waiting for a response by the API.
  self.isLoading = true

  // Make GET Request.
  return makeGETRequest(`/api/books/${self.queryParams}`).then((info) => {
    self.books = (self.page === 1) ? info.data : self.books.concat(info.data)
  }).catch(console.error).then(() => {
    // Indicate that the response by the API has been retrieved.
    self.isLoading = false
    // Clear TimeoutID.
    self.timeoutID = null
  })
}

/**
 * Function used to make a GET Request on a URL.
 * @param  {string} url       URL that the GET Request will be made on.
 * @return {Promise.<Object>} When the request is successful, it returns a
 *                            promise resolved with the response content.
 * @return {Promise.<Error>}  When the request is not successful, it returns a
 *                            rejected promise.
 */
function makeGETRequest (url) {
  return new Promise((resolve, reject) => {
    const xhr = new window.XMLHttpRequest()
    xhr.addEventListener('load', () => resolve(parseJSON(xhr.responseText)))
    xhr.addEventListener('error', () => reject(new Error('woops')))
    xhr.open('GET', url)
    xhr.send()
  })
}

/**
 * Function used to handle an update done to the sort info.
 * @param  {Object} info Stores sort info.
 * @param  {Object} self App's root view model.
 */
function handleSortChange (info, self) {
  switch (info.type) {
    case 'order': self.sort.order = info.data; break
    case 'field': self.sort.field = info.data; break
  }
}

/**
 * Function used to handle an update done to the filter info.
 * @param  {Object} info Stores filter info.
 * @param  {Object} self App's root view model.
 */
function handleFilterChange (info, self) {
  switch (info.type) {
    case 'value': self.filter.value = info.data; break
    case 'field': self.filter.field = info.data; break
  }
}

/**
 * Function used to safely parse a string into a JavaScript object.
 * @param  {string} str String to be parsed into a JavaScript object.
 * @return {Object}     Object representing the stringified object in the string
 *                      provided, if it is valid.
 * @return {Object}     Empty object if the string provided isn't a valid JSON
 *                      string.
 */
function parseJSON (str) {
  try {
    return JSON.parse(str)
  } catch (e) {
    return {}
  }
}

console.log(app)
