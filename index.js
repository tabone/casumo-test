'use strict'

const express = require('express')
const config = require('./config')
const routes = require('./routes')

// Create a new ExpressJS App.
const app = express()

console.info('mounting middlewares')
routes.forEach((router) => {
  console.info(`mounting '${router.endpoint}'`)
  app.use(router.endpoint, router.router)
})

// Start listening for HTTP Requests.
app.listen(config.server.port, config.server.host, () => {
  console.info(`listening on ${config.server.host}:${config.server.port}`)
})
