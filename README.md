# Book List Application

## Configuration

The app can be configured using `config.json` located in the root directory. Possible configurations:

Config      | Description
------------|------------
server.port | Port of server hosting the app.
server.host | Host of server hosting the app.
data.amount | Number of books to create.

## Start Application

```
  npm install
  npm start
```

## Comments

When starting the app (i.e. `npm start`), a list of books will be created &
stored in memory to be served on a newly created ExpressJS server. This server will be setup to serve the following 3 routes:

Endpoint    | Description
----------- | -----------
/           | Serving the frontend resources packaged by webpack.
/api/books  | Used to list, filter, sort & paginate the list of generated books.
/api/health | Used to check the health of the sever.

> The implementation of these endpoints can be seen in `/routes` directory.

### Technologies used in frontend app:
 - [VueJS](https://vuejs.org/) for the JavaScript framework.
 - SASS for Styling.
 - [BEM](http://getbem.com/) for CSS naming convention.

 > The implementation of the frontend app can be seen in `/src` directory.

### Features

- Sort by Book title & Author name.
- Filter by Book title, Book genre, Author name & Author gender.
- Author's gender is indicated by the color of his/her name.
- Horror books published on Halloween are indicated with an orage border.
- Finance books published on the last Friday of the month are indicated with a
blue border.
- Scrolling all the way down requests the next 10 books.

## Other notes

It took me around 15 hours.
