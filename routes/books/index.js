'use strict'

const data = require('./data')
const utils = require('./utils')

/**
 * Middleware used to serve info about the generated books. It also supports
 * query parameters to filter, sort & paginate books.
 * @param  {Object} req HTTP Request.
 * @param  {Object} res HTTP Response.
 */
module.exports = (req, res) => {
  // Filter data.
  let clone = utils.filter(req.query, data)

  // Sort data.
  clone = utils.sort(req.query, clone)

  // Retrieve the number of records before performing pagination.
  const totalRecords = clone.length

  // Paginate data.
  clone = utils.paginate(req.query, clone)

  // Serve the filtered, sorted & paginated data.
  res.json({ totalRecords, data: clone })
}
