'use strict'

/**
 * Stores utilities used by the books endpoint.
 * @type {Object}
 */
module.exports = {
  sort: require('./sort'),
  filter: require('./filter'),
  paginate: require('./paginate')
}
