'use strict'

/**
 * Function used to paginate the list of books. This function uses the
 * ?page[offset] & ?page[limit] query parameters to determine the offset & the
 * number of books to return respectively.
 * @param  {Object}         qs    Stores info about the query parameters.
 * @param  {Array.<Object>} books List of books to be paginated.
 * @return {Array.<Object>}       Paged list of books.
 */
module.exports = (qs, books) => {
  /**
   * Retrieve the pagination limit from the query string, else default it to 10.
   * @type {number}
   */
  const limit = (qs && qs.page && Number(qs.page.limit)) || 10

  /**
   * Retrieve the pagination offset from the query string, else default it to 0.
   * @type {number}
   */
  const offset = (qs && qs.page && Number(qs.page.offset)) || 0

  // Paginate & return the list of books.
  return books.splice(offset, limit)
}
