'use strict'

/**
 * Function used to sort the list of books. This function uses
 * ?sort[book][title]=order & ?sort[author][name]=order query parameters to sort
 * by the books title or books author name respectively.
 * @example
 * HTTP GET ...?sort[book][title]=asc
 * HTTP GET ...?sort[book][title]=desc
 * @param  {Object}         qs    Stores info about the query parameters.
 * @param  {Array.<Object>} books List of books to be sorted.
 * @return {Array.<Object>}       List of sorted books.
 */
module.exports = (qs, books) => {
  // Parse query params & retrieve the relative info.
  const info = parseQueryParams(qs)

  // Sort books
  books.sort((bookA, bookB) => {
    if (info.book.title != null) {
      if (bookA.title < bookB.title) return (info.book.title === true) ? -1 : 1
      if (bookA.title > bookB.title) return (info.book.title === true) ? 1 : -1
    }

    if (info.author.name != null) {
      if (bookA.author.name < bookB.author.name) {
        return (info.author.name === true) ? -1 : 1
      }

      if (bookA.author.name > bookB.author.name) {
        return (info.author.name === true) ? 1 : -1
      }
    }

    return 0
  })

  // Return sorted books.
  return books
}

/**
 * Function used to parse the query parameters to retrieve the sort info to be
 * used by this utility.
 * @param  {Object} qs Stores info about the query parameters.
 * @return {Object}    Stores the sort info to be used by this utility.
 */
function parseQueryParams (qs) {
  return {
    book: {
      title: qs.sort && qs.sort.book && qs.sort.book.title === 'asc'
    },
    author: {
      name: qs.sort && qs.sort.author && qs.sort.author.name === 'asc'
    }
  }
}
