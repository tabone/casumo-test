'use strict'

/**
 * Function used filter the list of books using the following query parameters:
 *   > ?filter[book][title] to filter by the book's name.
 *   > ?filter[book][genre] to filter by the book's genre.
 *   > ?filter[author][name] to filter by the book's author name.
 *   > ?filter[author][gender] to filter by the book's author gender.
 * @param  {Object}         qs    Stores info about the query parameters.
 * @param  {Array.<Object>} books List of books to be filtered.
 * @return {Array.<Object>}       Filtered list of books.
 */
module.exports = (qs, books) => {
  // Parse query params & retrieve the relative info.
  var info = parseQueryParams(qs)

  // Stop process & return a clone of the list of books, if no filter is
  // specified on the fields that can be filtered.
  if (isFilterUsed(info) === false) return books.slice(0)

  // Filter & return the list of books.
  return books.filter((book) => {
    return doesBookTitleMatch(info, book) === true ||
      doesBookGenreMatch(info, book) === true ||
      doesAuthorNameMatch(info, book) === true ||
      doesAuthorGenderMatch(info, book) === true
  })
}

/**
 * Function used to parse the query parameters to retrieve the filter info to be
 * used by this utility.
 * @param  {Object} qs Stores info about the query parameters.
 * @return {Object}    Stores the filter info to be used by this utility.
 */
function parseQueryParams (qs) {
  /**
   * Stores the filter info to be used by this utility.
   * @type {Object}
   */
  const info = {
    book: {
      title: (qs.filter && qs.filter.book && qs.filter.book.title) || [],
      genre: (qs.filter && qs.filter.book && qs.filter.book.genre) || []
    },
    author: {
      name: (qs.filter && qs.filter.author && qs.filter.author.name) || [],
      gender: (qs.filter && qs.filter.author && qs.filter.author.gender) || []
    }
  }

  // Normalize filter values to array.
  info.book.title = Array.isArray(info.book.title) === true
    ? info.book.title : [ info.book.title ]

  info.book.genre = Array.isArray(info.book.genre) === true
    ? info.book.genre : [ info.book.genre ]

  info.author.name = Array.isArray(info.author.name) === true
    ? info.author.name : [ info.author.name ]

  info.author.gender = Array.isArray(info.author.gender) === true
    ? info.author.gender : [ info.author.gender ]

  // Return filter info.
  return info
}

/**
 * Function used to determine whether there is any filtering to be made.
 * @param  {Object}  info Stores the filter info extracted from the query
 *                        parameters.
 * @return {boolean}      TRUE if the user has specified filters on the fields
 *                        that can be filtered.
 * @return {boolean}      FALSE if the user has not specified filters on the
 *                        fields that can be filtered.
 */
function isFilterUsed (info) {
  return info.book.title.length !== 0 || info.book.genre.length !== 0 ||
    info.author.name.length !== 0 || info.author.gender.length !== 0
}

/**
 * Function used to check whether the book's title match one of the book title
 * filters specified by the user.
 * @param  {Object} info Stores the filter info extracted from the query
 *                       parameters.
 * @param  {Object} book Book to have its title checked.
 * @return {boolean}     TRUE if the title of the specified book match one of
 *                       the book title filters specified by the user.
 * @return {boolean}     FALSE if the title of the specified book does not match
 *                       any of the book title filters specified by the user.
 */
function doesBookTitleMatch (info, book) {
  return info.book.title.find((filter) => {
    const regexp = new RegExp(filter, 'i')
    return regexp.test(book.title)
  }) !== undefined
}

/**
 * Function used to check whether the a book's genre match one of the book genre
 * filters specified by the user.
 * @param  {Object} info Stores the filter info extracted from the query
 *                       parameters.
 * @param  {Object} book Book to have its genre checked.
 * @return {boolean}     TRUE if the genre of the specified book match one of
 *                       the book genre filters specified by the user.
 * @return {boolean}     FALSE if the genre of the specified book does not match
 *                       any of the book genre filters specified by the user.
 */
function doesBookGenreMatch (info, book) {
  return info.book.genre.find((filter) => {
    return book.genre.toLowerCase() === filter.toLowerCase()
  }) !== undefined
}

/**
 * Function used to check whether the a book's author name match one of the
 * book's author name filters specified by the user.
 * @param  {Object} info Stores the filter info extracted from the query
 *                       parameters.
 * @param  {Object} book Book to have its author's name checked.
 * @return {boolean}     TRUE if the author's name of the specified book match
 *                       one of the book author's name filters specified by the
 *                       user.
 * @return {boolean}     FALSE if the author's name of the specified book does
 *                       not match any of the book author's name filters
 *                       specified by the user.
 */
function doesAuthorNameMatch (info, book) {
  return info.author.name.find((filter) => {
    const regexp = new RegExp(filter, 'i')
    return regexp.test(book.author.name)
  }) !== undefined
}

/**
 * Function used to check whether the a book's author gender match one of the
 * book's author gender filters specified by the user.
 * @param  {Object} info Stores the filter info extracted from the query
 *                       parameters.
 * @param  {Object} book Book to have its author's gender checked.
 * @return {boolean}     TRUE if the author's gender of the specified book match
 *                       one of the book author's gender filters specified by
 *                       the user.
 * @return {boolean}     FALSE if the author's gender of the specified book does
 *                       not match any of the book author's gender filters
 *                       specified by the user.
 */
function doesAuthorGenderMatch (info, book) {
  return info.author.gender.find((filter) => {
    return book.author.gender.toLowerCase() === filter.toLowerCase()
  }) !== undefined
}
