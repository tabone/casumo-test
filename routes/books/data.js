'use strict'

const faker = require('faker')
const config = require('../../config')

/**
 * List of possible genres.
 * @type {Array.<string>}
 */
const genres = [ 'Horror', 'Finance', 'Fiction', 'Satire', 'Romance' ]

/**
 * Array storing the list of books to be used by this app.
 * @example
 * [{
 *   genre: 'Fiction',
 *   image: 'http://...',
 *   publishedDate: '20141028',
 *   title: 'The World of Ice & Fire',
 *   author: { gender: 'M', name: 'George R.R. Martin' }
 * }]
 * @type {Array.<Object>}
 */
const data = []

// Let there be `data`...
console.info(`generating '${config.data.amount}' books...`)
for (let id = 0; data.length < config.data.amount; id++) {
  // Calculate percentage completed.
  const completed = id / config.data.amount * 100

  // Log percentage if it is divisible by 10.
  if (completed % 10 === 0) console.log(`${completed}% complete`)

  // Create and include a book in the list of data to be served by the API.
  data.push({
    id,
    image: faker.image.avatar(),
    title: faker.commerce.productName(),
    publishedDate: faker.date.past(),
    genre: genres[ Math.floor(Math.random() * genres.length) ],
    author: {
      gender: (data.length % 2) ? 'M' : 'F',
      name: `${faker.name.firstName()} ${faker.name.lastName()}`
    }
  })
}

module.exports = data
