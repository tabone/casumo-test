'use strict'

const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')

/**
 * Middleware used to serve the resources packaged by webpack.
 * @param  {Object} req HTTP Request.
 * @param  {Object} res HTTP Response.
 */
module.exports = webpackDevMiddleware(webpack(require('../../webpack.config')))
