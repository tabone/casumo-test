'use strict'

/**
 * Stores info about the routes that should be mounted to the main ExpressJS
 * app.
 * @type {Array.<Object>}
 */
module.exports = [{
  endpoint: '/',
  router: require('./webpack')
}, {
  endpoint: '/api/books',
  router: require('./books')
}, {
  endpoint: '/api/health',
  router: require('./health')
}]
