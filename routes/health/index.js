'use strict'

/**
 * Middleware used to check the health of the api.
 * @param  {Object} req HTTP Request.
 * @param  {Object} res HTTP Response.
 */
module.exports = (req, res) => res.json({ status: 'okay' })
