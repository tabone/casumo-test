'use strict'

const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

/**
 * Webpack configuration.
 * @type {Object}
 */
module.exports = {
  /**
   * This is the file from where webpack will start the bundling process.
   * @type {string}
   */
  entry: './src',

  /**
   * Stores info about the file that webpack will be creating, containing the
   * bundled app.
   * @type {Object}
   */
  output: {
    /**
     * Name of the bundle file.
     * @type {string}
     */
    filename: 'app.js',

    /**
     * Path where the bundle file will be created at.
     * @type {string}
     */
    path: path.join(__dirname, 'dist')
  },

  /**
   * Config used to instruct webpack to create source maps.
   * @type {string}
   */
  devtool: 'source-map',

  /**
   * Stores info about the resolution of modules.
   * @type {Object}
   */
  resolve: {
    /**
     * Stores aliases to modules.
     * @type {Object}
     */
    alias: {
      vue: path.join(__dirname, 'node_modules/vue/dist/vue.min.js')
    }
  },

  /**
   * Stores third party plugins that webpack will be using while bundling the
   * app.
   * @type {Array}
   */
  plugins: [
    /**
     * Plugin used to create the app's main HTML file containing a script tag
     * loading the bundled file.
     * @type {Object}
     */
    new HtmlWebpackPlugin({
      template: path.join(__dirname, './src/index.html')
    })
  ],

  /**
   * Stores info about how different resources should be treated by webpack.
   * @type {Object}
   */
  module: {
    rules: [
      // Loader used to Support ES6.
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: { presets: [ 'env' ] }
      },

      // Loader used to load HTML in JS files.
      {
        test: /\.(html)$/,
        loader: 'html-loader'
      },

      // Set of loaders used to:
      //   1. Compile SASS to CSS.
      //   2. Include CSS in JS files.
      //   3. Include CSS to the web page when the JS file is loaded.
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' }
        ]
      }
    ]
  }
}
